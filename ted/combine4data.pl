#!/usr/bin/perl
use strict;

my $data;

open(my $fh, "<", '../dataset/TBN_Y_ZERO.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CID} = 1;
}
close($fh);

open(my $fh, "<", '../dataset/TBN_CIF.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CIF} = join(',', map{ int $_ } @row[1 .. $#row]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_CC_APPLY.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CC} = int($row[1]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_FX_TXN.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{FX} = int($row[1]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_LN_APPLY.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{LN} = int($row[1]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_WM_TXN.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{WM} = int($row[1]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_RECENT_DT.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{RECENT_CC} = int($row[1]);
    $data->{$row[0]}{RECENT_FX} = int($row[2]);
    $data->{$row[0]}{RECENT_LN} = int($row[3]);
    $data->{$row[0]}{RECENT_WM} = int($row[4]);
}
close($fh);

my $outCC;
open($outCC, ">", "../data_CC.csv");
print $outCC "CUST_NO,AGE,CHILDREN_CNT,CUST_START_DT,EDU_CODE,GENDER_CODE,INCOME_RANGE_CODE,WORK_MTHS,TXN_DT,CC_RECENT_DT,CC_IND\n";

my $outFX;
open($outFX, ">", "../data_FX.csv");
print $outFX "CUST_NO,AGE,CHILDREN_CNT,CUST_START_DT,EDU_CODE,GENDER_CODE,INCOME_RANGE_CODE,WORK_MTHS,TXN_DT,FX_RECENT_DT,FX_IND\n";

my $outLN;
open($outLN, ">", "../data_LN.csv");
print $outLN "CUST_NO,AGE,CHILDREN_CNT,CUST_START_DT,EDU_CODE,GENDER_CODE,INCOME_RANGE_CODE,WORK_MTHS,TXN_DT,LN_RECENT_DT,LN_IND\n";

my $outWM;
open($outWM, ">", "../data_WM.csv");
print $outWM "CUST_NO,AGE,CHILDREN_CNT,CUST_START_DT,EDU_CODE,GENDER_CODE,INCOME_RANGE_CODE,WORK_MTHS,TXN_DT,WM_RECENT_DT,WM_IND\n";
#open(my $outTrain, ">", '../train_CC.csv');
#print $outTrain "CUST_NO, AGE, CHILDREN_CNT, CUST_START_DT, EDU_CODE, GENDER_CODE, INCOME_RANGE_CODE, WORK_MTHS, TXN_DT,CC_RECENT_DT\n";

#open(my $outTest, ">", '../test_CC.csv');
#print $outTest "CUST_NO,AGE,CHILDREN_CNT,CUST_START_DT,EDU_CODE,GENDER_CODE,INCOME_RANGE_CODE,WORK_MTHS,TXN_DT,CC_RECENT_DT,CC_IND\n";

#my $randNum = int(rand(5));
#my $cnt = 0;
for my $r (keys %{$data}){
    next if(!$data->{$r}{CID});
    $data->{$r}{CIF} //= ',,,,,,';

    my $ind;
    for my $j (('CC','FX','LN','WM')){
        $data->{$r}{$j} //= 0;
        $data->{$r}{"RECENT_$j"} //= 0;
        $ind->{$j} = ($data->{$r}{$j} || $data->{$r}{"RECENT_$j"}) ? 1 : 0;

        print $outCC "$r,$data->{$r}{CIF},$data->{$r}{$j},$data->{$r}{'RECENT_'.$j},$ind->{$j}\n" if($j eq 'CC');
        print $outFX "$r,$data->{$r}{CIF},$data->{$r}{$j},$data->{$r}{'RECENT_'.$j},$ind->{$j}\n" if($j eq 'FX');
        print $outLN "$r,$data->{$r}{CIF},$data->{$r}{$j},$data->{$r}{'RECENT_'.$j},$ind->{$j}\n" if($j eq 'LN');
        print $outWM "$r,$data->{$r}{CIF},$data->{$r}{$j},$data->{$r}{'RECENT_'.$j},$ind->{$j}\n" if($j eq 'WM');

        #print $outTrain "$r,$data->{$r}{CIF},$data->{$r}{$_},$data->{$r}{"RECENT_$_"}\n" if($cnt % 5 != $randNum);
        #print $outTest "$r,$data->{$r}{CIF},$data->{$r}{$_},$data->{$r}{"RECENT_$_"},$ind->{$_}\n" if($cnt % 5 == $randNum);
    }

    #if($cnt % 5 == 0){
    #    $randNum = int(rand(5));
    #}
    #$cnt++;
}

close($outCC);
close($outFX);
close($outLN);
close($outWM);

#close($outTrain);
#close($outTest);
