#!/usr/bin/perl
use strict;

my @patternList = ('gygrt', 'fgmo', 'wgdqth', 'gpda', 's', 'edrn');
my $data;
open(my $fh, "<", 'combineCIFtradingRecordAll.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CIF} = join(',', @row[1 .. $#row]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_CUST_BEHAVIOR.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    if(defined($data->{$row[0]})){
        for my $aPattern (@patternList){
            $data->{$row[0]}{CUST_BEHAVIOR}{$aPattern} //= 0;
            if($row[2] =~/\/$aPattern\//){
                $data->{$row[0]}{CUST_BEHAVIOR}{$aPattern}++;
            }
        }
    }
}
close($fh);

my $cnt = 0;
for my $r (keys %{$data}){
    if($data->{$r}{CUST_BEHAVIOR}){
        print "$r, $data->{$r}{CIF}";
        for my $aPattern (@patternList){
            print ", $data->{$r}{CUST_BEHAVIOR}{$aPattern}";
        }
        print "\n";
    }else{
        $cnt++;
    }
}
print "沒瀏覽筆數: $cnt\n";

