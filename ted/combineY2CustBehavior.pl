#!/usr/bin/perl
use strict;

my $data;
open(my $fh, "<", '../dataset/TBN_Y_ZERO.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CUSTID} = 1;
}
close($fh);

open(my $fh, "<", '../dataset/TBN_CIF.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    if($data->{$row[0]}{CUSTID}){
        $data->{$row[0]}{CIF} = 1;
    }
}
close($fh);

open(my $fh, "<", '../dataset/TBN_CUST_BEHAVIOR.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    if($data->{$row[0]}{CUSTID}){
        $data->{$row[0]}{BEHAVIOR} = 1;
    }
}
close($fh);

my $cntY;
my $cntN;
for my $r (keys %{$data}){
    next if(!$data->{$r}{CUSTID});
    if($data->{$r}{CIF}){
        $cntY->{CIF}++;
    }else{
        $cntN->{CIF}++;
    }
    if($data->{$r}{BEHAVIOR}){
        $cntY->{BEHAVIOR}++;
    }else{
        $cntN->{BEHAVIOR}++;
    }
}
print "有CIF: $cntY->{CIF}, 沒CIF: $cntN->{CIF}\n";
print "有瀏覽: $cntY->{BEHAVIOR}, 沒瀏覽: $cntN->{BEHAVIOR}\n";

