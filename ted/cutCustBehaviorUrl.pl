#!/usr/bin/perl
use strict;

our $LV = 4;

my $data;
open(my $fh, "<", '../dataset/TBN_CUST_BEHAVIOR_TRAN.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    my @spUrl = split('/', $row[2]);
    next if(!$spUrl[3]);

    for (4 .. 7){
        $spUrl[$_] //= '';
        $spUrl[$_] ||= '';
    }

    trimSymbol(\@spUrl, '"$', 3, 7);
    $data->{$spUrl[3]}++ if($LV == 1); #Lv2
    $data->{$spUrl[3]}{$spUrl[4]}++ if($LV == 2); #Lv2
    $data->{$spUrl[3]}{$spUrl[4]}{$spUrl[5]}++ if($LV == 3); #Lv3
    $spUrl[6] = "$spUrl[6]/$spUrl[7]" if($spUrl[7]);
    $data->{$spUrl[3]}{$spUrl[4]}{$spUrl[5]}{$spUrl[6]}++ if($LV == 4); #Lv4
}
close($fh);

for my $k (keys %{$data}){
    print "$k, $data->{$k}\n" if($LV == 1); #Lv2
    next if($LV < 2);
    for my $k2 (keys %{$data->{$k}}){
        print "$k/$k2, $data->{$k}{$k2}\n" if($LV == 2); #Lv2
        next if($LV < 3);
        for my $k3 (keys %{$data->{$k}{$k2}}){
            print "$k/$k2/$k3, $data->{$k}{$k2}{$k3}\n" if($LV == 3); #Lv3
            next if($LV < 4);
            for my $k4 (keys %{$data->{$k}{$k2}{$k3}}){
                print "$k/$k2/$k3/$k4, $data->{$k}{$k2}{$k3}{$k4}\n" if($LV == 4); #Lv4
            }
        }
    }
}


sub trimSymbol{
    my ($words, $reg, $sta, $end) = @_;
    for my $c ($sta .. $end){
        if(@$words[$c] =~/$reg/){
            chop(@$words[$c]);
        }
    }
}
