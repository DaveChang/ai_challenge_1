#!/usr/bin/perl
use strict;

my $data;
open(my $fh, "<", 'TBN_CIF.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CIF} = join(',', @row[1 .. $#row]);
}
close($fh);

open(my $fh, "<", 'TBN_WM_TXN.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    if($data->{$row[0]}{CIF}){
        $data->{$row[0]}{WM_TXN} = 1;
    }
}
close($fh);

open(my $fh, "<", 'TBN_FX_TXN.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    if($data->{$row[0]}{CIF}){
        $data->{$row[0]}{FX_TXN} = 1;
    }
}
close($fh);

open(my $fh, "<", 'TBN_LN_APPLY.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    if($data->{$row[0]}{CIF}){
        $data->{$row[0]}{LN_APPLY} = 1;
    }
}
close($fh);

open(my $fh, "<", 'TBN_CC_APPLY.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    if($data->{$row[0]}{CIF}){
        $data->{$row[0]}{CC_APPLY} = 1;
    }
}
close($fh);

open(my $fh, ">", 'test.csv');
print $fh join(",","CUST_NO","AGE","CHILDREN_CNT","CUST_START_DT","EDU_CODE","GENDER_CODE","INCOME_RANGE_CODE","WORK_MTHS","CC","FX","LN","WM")."\n";
for my $u (keys %{$data}){
    next if(!$data->{$u}{CIF});
    print $fh join(",",
        $u,
        $data->{$u}{CIF},
        ($data->{$u}{CC_APPLY} || 0),
        ($data->{$u}{FX_TXN} || 0),
        ($data->{$u}{LN_APPLY} || 0),
        ($data->{$u}{WM_TXN} || 0),
    )."\n";
}
close($fh);
