#!/usr/bin/perl
use strict;
use POSIX qw(strftime);

my $record;
my @CIF_item_max = ('AGE','CHILDREN_CNT','EDU_CODE','INCOME_RANGE_CODE','WORK_MTHS');
my @CIF_item_min = ('AGEm','CHILDREN_CNTm','EDU_CODEm','INCOME_RANGE_CODEm','WORK_MTHSm');
map{ $record->{$_} = 99; } @CIF_item_min;

my $data;

open(my $fh, "<", '../dataset/TBN_Y_ZERO.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CID} = 1;
}
close($fh);

open(my $fh, "<", '../dataset/TBN_CIF.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    next if($row[0] =~/CUST_NO/);

    $row[1] = int $row[1];
    $row[2] = int $row[2];
    $row[4] = int $row[4];
    $row[6] = int $row[6];
    $row[7] = int $row[7];

    #找最大值
    $record->{AGE} = $row[1] if($record->{AGE} < $row[1]);
    $record->{CHILDREN_CNT} = $row[2] if($record->{CHILDREN_CNT} < $row[2]);
    $record->{EDU_CODE} = $row[4] if($record->{EDU_CODE} < $row[4]);
    $record->{INCOME_RANGE_CODE} = $row[6] if($record->{INCOME_RANGE_CODE} < $row[6]);
    $record->{WORK_MTHS} = $row[7] if($record->{WORK_MTHS} < $row[7]);

    #找最小值
    $record->{AGEm} = $row[1] if($record->{AGEm} > $row[1]);
    $record->{CHILDREN_CNTm} = $row[2] if($record->{CHILDREN_CNTm} > $row[2]);
    $record->{EDU_CODEm} = $row[4] if($record->{EDU_CODEm} > $row[4]);
    $record->{INCOME_RANGE_CODEm} = $row[6] if($record->{INCOME_RANGE_CODEm} > $row[6]);
    $record->{WORK_MTHSm} = $row[7] if($record->{WORK_MTHSm} > $row[7]);

    #正規化 除以最大值
    my $aa = sprintf("%.2f", $row[1] / 4);
    my $cc = sprintf("%.2f", $row[2] / 10);
    my $ee = sprintf("%.2f", $row[4] / 6);
    my $gg = ($row[5] =~/M/) ? 1 : 0;
    my $ii = sprintf("%.2f", $row[6] / 4);
    my $ww = sprintf("%.2f", $row[7] / 5);

    $data->{$row[0]}{CIF} = join(',',$aa,$cc);
    $data->{$row[0]}{CUST_START_DT} = $row[3];
    $data->{$row[0]}{CIF2} = join(',',$ee,$gg,$ii,$ww);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_CC_APPLY.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CC} = int($row[1]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_FX_TXN.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{FX} = int($row[1]);

    #FX_TXN_AMT
    my $FX_TXN_AMT = int($row[2]);
    $data->{$row[0]}{FX2} = int($row[2]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_LN_APPLY.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{LN} = int($row[1]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_WM_TXN.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{WM} = int($row[1]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_RECENT_DT.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{RECENT_CC} = int($row[1]);
    $data->{$row[0]}{RECENT_FX} = int($row[2]);
    $data->{$row[0]}{RECENT_LN} = int($row[3]);
    $data->{$row[0]}{RECENT_WM} = int($row[4]);
}
close($fh);

my $outCC;
open($outCC, ">", "../data_CC_normalized.csv");
print $outCC printTitle('CC');

my $outFX;
open($outFX, ">", "../data_FX_normalized.csv");
print $outFX printTitle('FX');

my $outLN;
open($outLN, ">", "../data_LN_normalized.csv");
print $outLN printTitle('LN');

my $outWM;
open($outWM, ">", "../data_WM_normalized.csv");
print $outWM printTitle('WM');

#open(my $outTrain, ">", '../train_CC.csv');
#print $outTrain "CUST_NO, AGE, CHILDREN_CNT, CUST_START_DT, EDU_CODE, GENDER_CODE, INCOME_RANGE_CODE, WORK_MTHS, TXN_DT,CC_RECENT_DT\n";

#open(my $outTest, ">", '../test_CC.csv');
#print $outTest "CUST_NO,AGE,CHILDREN_CNT,CUST_START_DT,EDU_CODE,GENDER_CODE,INCOME_RANGE_CODE,WORK_MTHS,TXN_DT,CC_RECENT_DT,CC_IND\n";

#my $randNum = int(rand(5));
#my $cnt = 0;
for my $r (keys %{$data}){
    next if(!$data->{$r}{CID});
    $data->{$r}{CIF} //= '0,0'; #XXX
    $data->{$r}{CIF2} //= '0,0,0,0'; #XXX

    my $ind;
    for my $j (('CC','FX','LN','WM')){
        $data->{$r}{$j} //= 0;
        $data->{$r}{'RECENT_'.$j} //= 0;
        $ind->{$j} = ($data->{$r}{$j} || $data->{$r}{"RECENT_$j"}) ? 1 : 0;

        #時間切割
        my $dateArr; #9542 -> 1522598400
        for my $item (($j, 'RECENT_'.$j, 'CUST_START_DT')){
            if($data->{$r}{$item} > 0){
                my $date = ($data->{$r}{$item}-9542)*86400+1522598400;
                my @time = gmtime($date);
                my $dateStr = strftime('%Y-%m-%d-%w', @time);
                @{$dateArr->{$item}} = split('-',$dateStr);
                ${$dateArr->{$item}}[0] = (${$dateArr->{$item}}[0] < 2008) ? 0 : (${$dateArr->{$item}}[0] - 2008) / 10;
                ${$dateArr->{$item}}[1] = sprintf("%.2f", ${$dateArr->{$item}}[1] / 12);
                ${$dateArr->{$item}}[2] = sprintf("%.2f", ${$dateArr->{$item}}[2] / 30);
                ${$dateArr->{$item}}[3] = sprintf("%.2f", ${$dateArr->{$item}}[3] / 7);
            }
            $dateArr->{$item} //= [0,0,0,0];
        }

        print $outCC "$r,$data->{$r}{CIF},".join(',', @{$dateArr->{CUST_START_DT}}).",$data->{$r}{CIF2},".join(',', @{$dateArr->{$j}}).",".join(',', @{$dateArr->{'RECENT_'.$j}}).",$ind->{$j}\n" if($j eq 'CC');
        print $outFX "$r,$data->{$r}{CIF},".join(',', @{$dateArr->{CUST_START_DT}}).",$data->{$r}{CIF2},".join(',', @{$dateArr->{$j}}).",".join(',',@{$dateArr->{'RECENT_'.$j}}).",$ind->{$j}\n" if($j eq 'FX');
        print $outLN "$r,$data->{$r}{CIF},".join(',', @{$dateArr->{CUST_START_DT}}).",$data->{$r}{CIF2},".join(',', @{$dateArr->{$j}}).",".join(',',@{$dateArr->{'RECENT_'.$j}}).",$ind->{$j}\n" if($j eq 'LN');
        print $outWM "$r,$data->{$r}{CIF},".join(',', @{$dateArr->{CUST_START_DT}}).",$data->{$r}{CIF2},".join(',', @{$dateArr->{$j}}).",".join(',',@{$dateArr->{'RECENT_'.$j}}).",$ind->{$j}\n" if($j eq 'WM');


        #print $outTrain "$r,$data->{$r}{CIF},$data->{$r}{$_},$data->{$r}{"RECENT_$_"}\n" if($cnt % 5 != $randNum);
        #print $outTest "$r,$data->{$r}{CIF},$data->{$r}{$_},$data->{$r}{"RECENT_$_"},$ind->{$_}\n" if($cnt % 5 == $randNum);
    }

    #if($cnt % 5 == 0){
    #    $randNum = int(rand(5));
    #}
    #$cnt++;
}

close($outCC);
close($outFX);
close($outLN);
close($outWM);

print "MAX\n";
for (@CIF_item_max){
    print "$_:$record->{$_}\n";
}
print "MIN\n";
for (@CIF_item_min){
    print "$_:$record->{$_}\n";
}

#close($outTrain);
#close($outTest);

sub printTitle {
    my ($type) = @_;
    return "CUST_NO,AGE,CHILDREN_CNT,CUST_START_DT_Y,CUST_START_DT_M,CUST_START_DT_D,CUST_START_DT_W,EDU_CODE,GENDER_CODE,INCOME_RANGE_CODE,WORK_MTHS,TXN_DT_Y,TXN_DT_M,TXN_DT_D,TXN_DT_W,${type}_RECENT_DT_Y,${type}_RECENT_DT_M,${type}_RECENT_DT_D,${type}_RECENT_DT_W,${type}_IND\n";
}
