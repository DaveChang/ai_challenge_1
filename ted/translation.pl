#!/usr/bin/perl
use strict;
our %dict;

importDict();


open(my $fh, "<", '../dataset/TBN_CUST_BEHAVIOR.csv');
while(<$fh>){
    chomp();
    my @row = split(',', $_);
    my @row2 = split('/', $row[2]);

    my @out;
    for my $r (@row2[3 .. $#row2]){
        my $res = translation($r);
        next if(!$r || !$res);
        push @out, $res;
    }
    print join(',', @row[0 .. 1], join("/", @row2[0 .. 2], @out))."\n";
}


sub translation {
    my ($word) = @_;

    my $res;
    if($dict{$word}){
        $res = $dict{$word};
    }else{
        my @w = split('', $word);
        for my $sw (@w){
            next if($sw =~/\s/g);
            if($dict{$sw}){
                $res .= $dict{$sw};
            }elsif($sw =~/[\d|\.|\_|\-|\"]/g){
                $res .= $sw;
            }else{
#                print "\n\nERROR $sw\n\n";
            }
        }
    }

    return $res;
}

sub importDict {
    open(my $fh, "<", 'translationList.csv');
    while (<$fh>){
        chomp();
        my @row = split(',', $_);
        $dict{$row[0]} = $row[1];
    }
    close($fh);
}

