#!/usr/bin/perl
use strict;

my $data;
open(my $fh, "<", '../dataset/TBN_CIF.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{CIF} = join(',', @row[1 .. $#row]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_RECENT_DT.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    $data->{$row[0]}{RECENT} = join(',', @row[1 .. $#row]);
}
close($fh);

open(my $fh, "<", '../dataset/TBN_Y_ZERO.csv');
while (<$fh>){
    chomp();
    my @row = split(',', $_);
    if($data->{$row[0]}{CIF}){
        delete $data->{$row[0]}{CIF};
    }
    if($data->{$row[0]}{RECENT}){
        delete $data->{$row[0]}{RECENT};
    }
}
close($fh);

my $cntCIF = 0;
my $cntCIFn = 0;
my $cntRECENT = 0;
my $cntRECENTn = 0;
for my $r (keys %{$data}){
    if (!$data->{$r}{CIF}){
        $cntCIFn++;
    }else{
        $cntCIF++;
    }
    if (!$data->{$r}{RECENT}){
        $cntRECENTn++;
    }else{
        $cntRECENT++;
    }
    #print "$r: $data->{$r}{CIF}, $data->{$r}{RECENT}\n";
}
print "CIF:$cntCIF, CIFn:$cntCIFn, RECENT:$cntRECENT, RECENTn:$cntRECENTn\n";
exit;

