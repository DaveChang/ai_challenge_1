#!/usr/local/bin/python3.6

import csv
import time

stage = '4'
def time_debug (msg):
    print('['+time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())+']'+msg)

def create_dict (filename):
    with open(filename, newline='') as csvfile:
        rows = csv.reader(csvfile) # 讀取 CSV 檔案內容
        count = 0;
        d = dict()
        for row in rows: # 以迴圈輸出每一列
            if row[0] == 'CUST_NO':
                continue
            if (row[0] in d.keys()):
                continue
            d[row[0]] = count
            count+=1
        return d

#raw_data = ['TBN_CC_APPLY', 'TBN_CUST_BEHAVIOR', 'TBN_FX_TXN', 'TBN_LN_APPLY', 'TBN_RECENT_DT', 'TBN_WM_TXN', 'TBN_Y_ZERO']
#raw_data = ['data_CC_normalized_v2'] #train 信用卡
#raw_data = ['data_FX_normalized_v2', 'data_LN_normalized_v2', 'data_WM_normalized_v2'] #train
#raw_data = ['TBN_Y_ZERO'] # result data
raw_data = ['td4'] # result data
def write_file (d):
    for data in raw_data:
        time_debug('目前處理資料:'+data)
        with open(data+'.csv', 'r+') as rf, open('../dataset_'+stage+'/'+data+'_'+stage+'.csv', 'w') as wf, open('../dataset_'+stage+'/'+data+'_'+stage+'_n.csv', 'w') as wnf:
            oriRows = csv.reader(rf) # 讀取 CSV 資料
            wRows = csv.writer(wf) # mapping 後 CSV 資料
            notMapping = csv.writer(wnf) # 無法mapping CSV 資料
            for oriRow in oriRows: # 迴圈輸出每一列
                if oriRow[0] == 'CUST_NO':
                    print(oriRow)
                    notMapping.writerow(oriRow)
                    wRows.writerow(oriRow)
                    continue

                if (oriRow[0] in d.keys()):
                    oriRow[0] = int(d[oriRow[0]])
                    wRows.writerow(oriRow)
                else:
                    notMapping.writerow(oriRow)

        time_debug('已處理完:'+data)

def main():
    loadData = '../dataset/TBN_Y_ZERO.csv'
    mappingDict = create_dict(loadData)
    write_file(mappingDict)

if __name__ == '__main__':
    main()  # 或是任何你想執行的函式
