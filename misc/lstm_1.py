# coding: utf-8
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

from subprocess import check_output
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential
from sklearn.cross_validation import  train_test_split
import time #helper libraries
from sklearn.preprocessing import MinMaxScaler
import matplotlib.pyplot as plt
from numpy import newaxis
from geopy.geocoders import Nominatim

get_ipython().run_line_magic('matplotlib', 'inline')
import warnings
warnings.filterwarnings('ignore')
get_ipython().run_line_magic('config', "InlineBackend.figure_format = 'retina'")
# Any results you write to the current directory are saved as output.

from subprocess import check_output
print(check_output(["ls", "./"]).decode("utf8"))


# In[7]:


cc_dataset =  pd.read_csv("../data_CC_normalized.csv", header=0)
cc_dataset.head()


# In[8]:


print(len(cc_dataset[["年齡", "小孩數", "開始年", "開始月", "開始日", "開始週", "教育程度", "性別", "年收入", "工作月數", "申請年", "申請月", "申請日", "申請週", "最近一次年", "最近一次月", "最近一次日", "最近一次週"]]))


# In[9]:


cc_dataset[["年齡", "小孩數", "開始年", "開始月", "開始日", "開始週", "教育程度", "性別", "年收入", "工作月數", "申請年", "申請月", "申請日", "申請週", "最近一次年", "最近一次月", "最近一次日", "最近一次週"]][1:2:2]


# In[10]:


final_dataset = cc_dataset[["年齡", "小孩數", "開始年", "開始月", "開始日", "開始週", "教育程度", "性別", "年收入", "工作月數", "申請年", "申請月", "申請日", "申請週", "最近一次年", "最近一次月", "最近一次日", "最近一次週"]]


# In[12]:


#train_size = int(len(final_dataset) * 0.80)
#test_size = len(final_dataset) - train_size
#train, test = final_dataset[0:train_size], final_dataset[train_size:len(final_dataset)]
#print(len(train), len(test))
train = final_dataset
print(len(train))

def buildTrain(train, pastDay=120, futureDay=30):
    X_train, Y_train = [], []
    for i in range(train.shape[0]-futureDay-pastDay):
        X_train.append(np.array(train.iloc[i:i+pastDay]))
        Y_train.append(np.array(train.iloc[i+pastDay:i+pastDay+futureDay]["申請日"]))
    return np.array(X_train), np.array(Y_train)

# In[13]:

# convert an array of values into a dataset matrix
#def create_dataset(dataset, look_back=1):
#    dataX, dataY = [], []
#        for i in range(len(dataset)-look_back-1):
#            a = dataset[i:(i+look_back)]
#                dataX.append(np.array(a))
#                dataY.append(np.array(dataset[i + look_back:i+look_back+1]))
#
#        return np.array(dataX),np.array(dataY)
#
#
## In[14]:
#
#look_back = 50
#trainX, trainY = create_dataset(train, look_back)

def shuffle(X,Y):
    np.random.seed(10)
    randomList = np.arange(X.shape[0])
    np.random.shuffle(randomList)
    return X[randomList], Y[randomList]

def splitData(X,Y,rate):
    X_train = X[int(X.shape[0]*rate):]
    Y_train = Y[int(Y.shape[0]*rate):]
    X_val = X[:int(X.shape[0]*rate)]
    Y_val = Y[:int(Y.shape[0]*rate)]
    return X_train, Y_train, X_val, Y_val


# In[15]:




# In[16]:


#Step 2 Build Model
def buildManyToOneModel(shape):
    model = Sequential()
    #model.reset_states()

    model.add(LSTM(
        input_dim=3,
        output_dim=100,
        ))

    model.add(Dropout(0.2))

    model.add(LSTM(100))

    model.add(Dropout(0.2))

    model.add(Dense(output_dim=3))

    model.add(Activation('linear'))

    start = time.time()
    model.compile(loss='mse', optimizer='adam')
    print ('compilation time : ', time.time() - start)
    model.summary()
    return model

# change the last day and next day
X_train, Y_train = buildTrain(train, 120, 30)
X_train, Y_train = shuffle(X_train, Y_train)
# because no return sequence, Y_train and Y_val shape must be 2 dimension
X_train, Y_train, X_val, Y_val = splitData(X_train, Y_train, 0.1)

print(X_train.shape)
print(Y_train.shape)

model = buildManyToOneModel(X_train.shape)
callback = EarlyStopping(monitor="loss", patience=10, verbose=1, mode="auto")
model.fit(X_train, Y_train, epochs=1000, batch_size=128, validation_data=(X_val, Y_val), callbacks=[callback])
