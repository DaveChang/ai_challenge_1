#!/usr/local/bin/python3.6

import csv
# 開啟 CSV 檔案
def create_dict (filename):
    with open(filename, newline='') as csvfile:
        rows = csv.reader(csvfile) # 讀取 CSV 檔案內容
        count = 0;
        d = dict()
        for row in rows: # 以迴圈輸出每一列
            if row[0] == 'CUST_NO':
                continue
            if (row[0] in d.keys()):
                continue
            d[row[0]] = count
            count+=1
        return d

def write_mapping_csv (d, out):
    with open(out, 'a') as f:
        w = csv.writer(f)
        w.writerow(['Name', 'No'])
        for data in d:
            w.writerow([data, d[data]])

def main():
    loadData = '../dataset_2/data_CC_normalized.csv'
    outFile = 'CC.csv'
    mappingDict = create_dict(loadData)
    write_mapping_csv(mappingDict, outFile)

if __name__ == '__main__':
    main()  # 或是任何你想執行的函式
